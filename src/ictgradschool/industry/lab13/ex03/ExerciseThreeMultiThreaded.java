package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    double estimateSum = 0;
    List<Thread> estimationThreads = new ArrayList<>();

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        // Implement this.

        long numThreads = 8;
        long sampleSize = numSamples/numThreads;
        long t4 = sampleSize * numThreads;

        for (long thread = 0; thread < numThreads; thread++) {
            initiateThread(sampleSize);
        } if (t4 != numSamples){
            initiateThread(numSamples - t4);
        }

        for (Thread t : estimationThreads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return result(numSamples);
    }

    private void initiateThread(long sampleSize){
        Thread calcThread = new Thread(new Runnable() {
            @Override
            public void run() {
                double partEstimate = ExerciseThreeMultiThreaded.super.estimatePI(sampleSize) * sampleSize;
                addThreadResult(partEstimate);
            }
        });
        calcThread.start();
        estimationThreads.add(calcThread);
    }

    private synchronized void addThreadResult(double estimate) {
        estimateSum += estimate;
    }

    private double result(long numSamples) {
        return estimateSum / numSamples;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
