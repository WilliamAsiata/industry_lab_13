package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by wasi131 on 2/05/2017.
 */
public class ConcurrentBankingApp {

    private ArrayBlockingQueue<Transaction> transactionArrayBlockingQueue;
    private List<Thread> consumerThreads;
    private List<Thread> producerThreads;
    private BankAccount account;
    private List<Transaction> transactions;

    public ConcurrentBankingApp(int queueCapacity, List<Transaction> transactions) {
        this.transactionArrayBlockingQueue = new ArrayBlockingQueue<Transaction> (queueCapacity);
        this.consumerThreads = new ArrayList<Thread>();
        this.producerThreads = new ArrayList<Thread>();
        this.account = new BankAccount();
        this.transactions = transactions;
    }

    class Consumer implements Runnable {
        @Override
        public void run() {
            Transaction transaction;

            try {
                while (true){
                    transaction = transactionArrayBlockingQueue.take();
                    switch (transaction._type) {
                        case Deposit:
                            account.deposit(transaction._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(transaction._amountInCents);
                    }
                }
            } catch (InterruptedException e) {
                System.out.println("consumerThread interrupted");
            }
        }
    }

    private void initiateProducer(){
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Transaction t : transactions){
                    try {
                        transactionArrayBlockingQueue.put(t);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (Thread consumer : consumerThreads){
                    consumer.interrupt(); // Ask consumerThreads to terminate gracefully.
                }
            }
        });
        producer.start();
        producerThreads.add(producer);
    }

    private void initiateConsumers(int numConsumers){
        for (int i = 0; i < numConsumers; i++){
            Thread consumer = new Thread(new Consumer());
            consumer.start();
            consumerThreads.add(consumer);
        }
    }

    public static void main(String[] args) {
        // Acquire Transactions to process.
        // Create BankAccount object to operate on.
        //Create transactions buffer queue & list of threads.
        ConcurrentBankingApp runApp = new ConcurrentBankingApp(10, TransactionGenerator.readDataFile());

        // For each Transaction, apply it to the BankAccount instance.
        runApp.initiateProducer();
        runApp.initiateConsumers(2);

        // Check consumerThreads have terminated before printing final balance.
        for (Thread t : runApp.consumerThreads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + runApp.account.getFormattedBalance());
    }
}
