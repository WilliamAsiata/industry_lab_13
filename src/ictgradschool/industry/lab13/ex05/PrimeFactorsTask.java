package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

import java.util.*;

/**
 * Created by wasi131 on 2/05/2017.
 */
public class PrimeFactorsTask extends PrimeFactors {

    private List<Long> list;
    private Taskstate taskstate;

    enum Taskstate {Initialised, Completed, Aborted}
    long n(){return n;}
    Taskstate getState(){return taskstate;}

    PrimeFactorsTask(long n){
        super(n);
        this.list = new ArrayList<>();
        this.taskstate = Taskstate.Initialised;
    }

    @Override
    public void run() {
        try {
            list.addAll(computeResult());
            taskstate = Taskstate.Completed;

        } catch (InterruptedException e) {
            System.out.println("Calculation aborted.");
            taskstate = Taskstate.Aborted;
        }
    }

    List<Long> getPrimeFactors () throws IllegalStateException {
        if (taskstate == Taskstate.Completed) return list;
        else throw new IllegalStateException();
    }

    public static void main(String[] args) {

        System.out.println("Find prime factors of number:");
        PrimeFactorsTask task = new PrimeFactorsTask(Long.parseLong(Keyboard.readInput()));
        Thread taskThread = new Thread(task);
        taskThread.start();

        Thread interruptSignal = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Press enter to abort calculation");
                Keyboard.readInput();
                taskThread.interrupt();
            }
        });
        interruptSignal.setDaemon(true); // Dies if it is the last thread running in program (after main finishes in this case).
        interruptSignal.start();

        try {
            taskThread.join();
//            interruptSignal.stop(); // <-- use setDaemon() instead.
//            System.out.println(interruptSignal.getState());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("");

        try{
            System.out.println("The prime factors are: " + task.getPrimeFactors());
        } catch (IllegalStateException e) {
            System.out.println("Calculation not completed.");
        }
    }
}